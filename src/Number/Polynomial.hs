{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE FlexibleContexts #-}

module Number.Polynomial ( Poly
                  , Polynomial
                  , poly
                  , coefficients
                  , degree
                  , leading
                  , identity
                  , constant
                  , x, y, z
                  , Endofunction
                  , evaluate
                  , compose )
                  where

import Number.Poly

import Data.Ord (comparing)
import Data.Ratio ((%))
import GHC.Real (Ratio((:%)), Fractional)
import Data.Kind (Type)
import Data.List (intercalate, transpose)

type Polynomial = Poly Rational
type BivariatePolynomial = Poly Polynomial

instance (Eq s, Num s, Show s) => Show (Poly s) where
  show = show . coefficients
  -- show 0 = show 0
  -- show a = intercalate " + "
  --        $ map (\(n,x) -> show n ++ x)
  --        $ filter ((/=0) . fst)
  --        $ zip (coefficients a) $ "" : "x" : ["x^" ++ show i | i <- [2..]]

degree :: Poly a -> Int
degree = pred . length . coefficients

leading :: Poly a -> a
leading = last . coefficients

x :: Endofunction p => p
y :: (Endofunction p, Endofunction (Domain p)) => p
z :: (Endofunction p, Endofunction (Domain p), Endofunction (Domain (Domain p))) => p
x = identity
y = constant x
z = constant y

class Endofunction p where
  type Domain p :: Type
  evaluate :: p -> Domain p -> Domain p
  compose :: p -> p -> p
  identity :: p
  constant :: Domain p -> p

instance (Eq a, Num a) => Endofunction (Poly a) where
  type Domain (Poly a) = a
  evaluate a c = sum (zipWith (*) (iterate (*c) 1) (coefficients a))
  compose = evaluate . polynomialize
  identity = poly [0,1]
  constant = poly . pure

polynomialize :: (Num a, Eq a) => Poly a -> Poly (Poly a)
polynomialize = poly . fmap (poly . pure) . coefficients

rationalize :: Integral a => Poly a -> Poly (Ratio a)
rationalize = poly . fmap (% 1) . coefficients

elementwise :: (Num a, Num b, Num c, Eq c) => (a -> b -> c) -> Poly a -> Poly b -> Poly c
elementwise f a b = poly $ zipWith0 f (coefficients a) (coefficients b)

(./) :: (Num a, Eq a, Fractional a) => Poly a -> Poly a -> Poly a
(./) = elementwise (/)

(.%) :: (Integral a) => Poly a -> Poly a -> Poly (Ratio a)
(.%) = elementwise (%)

instance (Integral a) => Endofunction (Ratio (Poly a)) where
  type Domain (Ratio (Poly a)) = Ratio a
  evaluate (n :% d) c = (evaluate.rationalize) n c
                      / (evaluate.rationalize) d c
  compose (n1 :% d1) (n2 :% d2) = undefined
    where asdf = compose n1 n2 .% compose n1 d2
          jkll = (rationalize.polynomialize) n1
  -- identity = identity % 1
  -- constant (n :% d) = constant n % constant d

instance (Eq n, Num n) => Num (Poly n) where
  a + b = poly $ map sum $ transpose [coefficients a, coefficients b]
  a * b = poly . map sum $ shearTranspose (map (\i -> map (*i) (coefficients a)) (coefficients b))
  abs f = signum f * f
  signum 0 = 0
  signum _ = 1
  negate a = poly (negate <$> coefficients a)
  fromInteger n = poly [fromInteger n]

instance (Eq n, Num n, Enum n) => Enum (Poly n) where
  toEnum n = poly [fromIntegral n]
  fromEnum a = fromEnum (head (coefficients a ++ [0]))

instance Eq n => Eq (Poly n) where
  a == b = coefficients a == coefficients b

instance Eq n => Ord (Poly n) where
  compare = comparing (length . coefficients)

instance (Eq n, Num n) => Real (Poly n) where
  toRational = error "Polynomial is no rational"

instance (Enum n, Ord n, Fractional n) => Integral (Poly n) where
  quotRem a b = go 0 a
    where go q r | degree r >= degree b && leading r >= leading b
                 = let s = poly . reverse $ leading r / leading b : replicate (degree r - degree b) 0
                   in go (q + s) (r - s*b)
                 | otherwise = (q,r)
  toInteger = error "Polynomial is no integer"

instance (Eq n, Num n) => Fractional (Poly n) where
  recip a = poly [1] / a
  fromRational = error "Polynomial is no rational"

-- instance (Eq n, Num n, Enum n) => Integral (Poly n) where
--   quotRem a b = (poly $ map fromIntegral $ coefficients a, poly $ map fromIntegral $ coefficients b)
--   toInteger = error "Polynomial is no integer"

shearTranspose :: [[a]] -> [[a]]
shearTranspose = foldr zipConsSkew []

zipConsSkew :: [a] -> [[a]] -> [[a]]
zipConsSkew xt yss =
   uncurry (:) $
   case xt of
      x:xs -> ([x], zipCons' xs yss)
      [] -> ([], yss)

zipCons' :: [a] -> [[a]] -> [[a]]
zipCons' (x:xs) (y:ys) = (x:y) : zipCons' xs ys
zipCons' [] ys = ys
zipCons' xs [] = map (:[]) xs

zipWith0 :: (Num a, Num b) => (a -> b -> c) -> [a] -> [b] -> [c]
zipWith0 f [] [] = []
zipWith0 f (x:xs) [] = f x 0 : zipWith0 f xs []
zipWith0 f [] (y:ys) = f 0 y : zipWith0 f [] ys
zipWith0 f (x:xs) (y:ys) = f x y : zipWith0 f xs ys