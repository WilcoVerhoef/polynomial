module Number.Poly (poly, Poly (..)) where

import Data.List

newtype Poly n = Poly { coefficients :: [n] }

poly :: (Num n, Eq n) => [n] -> Poly n
poly = Poly . trimZeroes . group
  where trimZeroes [] = []
        trimZeroes [0:_] = []
        trimZeroes (n:ns) = n ++ trimZeroes ns
